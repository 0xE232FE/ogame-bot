FROM golang:1.13 as build

ENV GO111MODULE=on
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64

WORKDIR /
COPY . .

RUN go build -o ogame-bot src/cmd/ogame-bot/main.go

FROM alpine:latest
COPY --from=build /.ogame-bot.yaml .
COPY --from=build /ogame-bot .

ENTRYPOINT ["./ogame-bot"]
CMD ["-c", ".ogame-bot.yaml"]
