module gitlab.com/lilflip/ogame-bot

go 1.13

require (
	github.com/bwmarrin/discordgo v0.20.2
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.6
	github.com/spf13/viper v1.6.2
)
