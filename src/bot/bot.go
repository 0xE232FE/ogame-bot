package bot

import (
	"regexp"
	"strings"

	"github.com/bwmarrin/discordgo"
	cfg "gitlab.com/lilflip/ogame-bot/src/configuration"
)

var cmdPrefix = "og-"
var botCommands = make(map[string]BotCommand)

type botCommandHandler func(s *discordgo.Session, m *discordgo.MessageCreate)

const (
	defaultArgsMode = "00"
	noArgsMode      = "01"
	requireArgsMode = "10"
)

type BotCommand struct {
	Name        string
	Description string
	ArgsMode    string
	//Flags       []Flag

	handler  botCommandHandler
	channels map[string]bool
}

/*
type Flag struct {
	Name        string
	Description string
	Short       string
	Long        string
}
*/

func HandleMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	for _, botCommand := range botCommands {
		if botCommand.Match(m.Content) {
			botCommand.Handle(s, m)
		}
	}
}

func (botCommand *BotCommand) setHandler(handler botCommandHandler) {
	botCommand.handler = handler
}

func (botCommand BotCommand) Handle(s *discordgo.Session, m *discordgo.MessageCreate) {
	if botCommand.Available(m.ChannelID) {
		botCommand.handler(s, m)
	}
}

func (botCommand BotCommand) Register() {
	botCommands[botCommand.Name] = botCommand
}

func isRegistered(strBotCommand string) bool {
	_, ok := botCommands[strBotCommand]
	return ok
}

func (botCommand BotCommand) regexp() string {
	return "!" + cmdPrefix + botCommand.Name + " "
}

func (botCommand BotCommand) regexpNoArgs() string {
	return "!" + cmdPrefix + botCommand.Name
}

func (botCommand BotCommand) GetArgs(command string) []string {
	var args []string
	switch botCommand.ArgsMode {
	case noArgsMode:
		break
	case requireArgsMode:
		args = strings.Split(strings.TrimPrefix(command, botCommand.regexp()), " ")
	default:
		args = strings.Split(strings.TrimPrefix(command, botCommand.regexp()), " ")
		if args[0] == command {
			args = []string{}
		}
	}

	return args

}

func (botCommand BotCommand) Match(command string) bool {

	var match bool

	switch botCommand.ArgsMode {
	case noArgsMode:
		match, _ = regexp.MatchString(botCommand.regexpNoArgs(), command)
		break
	case requireArgsMode:
		match, _ = regexp.MatchString(botCommand.regexp(), command)
		break
	default:
		match, _ = regexp.MatchString(botCommand.regexp(), command)
		if match {
			break
		}
		match, _ = regexp.MatchString(botCommand.regexpNoArgs(), command)
	}

	return match

}

func (botCommand BotCommand) Available(channelID string) bool {

	// so init does not fuck us up

	var channelIDsMap = make(map[string]bool)
	for _, channelID := range cfg.Global.GetStringSlice("commands." + botCommand.Name + ".channel_ids") {
		channelIDsMap[channelID] = true
	}

	botCommand.channels = channelIDsMap

	//

	if botCommand.channels["*"] == true {
		return true
	}
	return botCommand.channels[channelID]

}

/*
func (botCommand BotCommand) AddToChannel(channelID string) {
	botCommand.channels[channelID] = true
}

func (botCommand BotCommand) RemoveFromChannel(channelID string) {
	delete(botCommand.channels, channelID)
}
*/
