package bot

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

var cmdHelp = BotCommand{
	Name:        "help",
	Description: "0 / 1 str arg in commands list",
	ArgsMode:    defaultArgsMode,
	channels:    make(map[string]bool),
}

func init() {
	cmdHelp.setHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {

		var args = cmdHelp.GetArgs(m.Content)

		if len(args) > 0 {
			if isRegistered(args[0]) {
				s.ChannelMessageSend(m.ChannelID, "help "+args[0])
			}
		} else {
			var helpCmdMessage = "Usage " + cmdPrefix + "[COMMAND]:\n	available commands:\n"
			for _, command := range botCommands {
				var commandMessage = fmt.Sprintf("		%s: %s\n", command.Name, command.Description)
				helpCmdMessage = fmt.Sprintf("%s%s", helpCmdMessage, commandMessage)

			}
			s.ChannelMessageSend(m.ChannelID, helpCmdMessage)
		}

	})

	cmdHelp.Register()
}
