package bot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/lilflip/ogame-bot/src/trashsim"
)

var cmdRaid = BotCommand{
	Name:        "raid",
	Description: "1 str arg player to raid",
	ArgsMode:    requireArgsMode,
	channels:    make(map[string]bool),
}

func init() {
	cmdReport.setHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {

		var args = cmdHelp.GetArgs(m.Content)

		var mapRequestBody, _ = json.Marshal(map[string]interface{}{
			"key":   args[1],
			"party": "defenders",
			"fleet": 2,
		})

		var requestBody = bytes.NewBuffer(mapRequestBody)

		response, err := http.Post(
			"https://trashsim.universeview.be/api/player",
			"application/json",
			requestBody,
		)
		if err != nil {
			log.Print(err.Error())
			s.ChannelMessageSend(m.ChannelID, err.Error())
		}

		defer response.Body.Close()

		responseBody, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Print(err.Error())
			s.ChannelMessageSend(m.ChannelID, err.Error())
		}

		var structJSON = new(trashsim.Payload)

		json.Unmarshal([]byte(responseBody), structJSON)

		var formattedMessage = fmt.Sprintf(`

		Player: %s
		Coord: %s
		Class: %s

		Builldings:

		Mine de métal: %d
		Mine de cristal: %d
		Synthétiseur de deutérium: %d

		Centrale éléctrique solaire: %d
		Centrale éléctrique de fusion: %d

		Hangar de métal: %d
		Hangar de cristal: %d
		Réservoir de deutérium: %d

		Usine de robots: %d
		Chantier spatial: %d
		Laboratoire de recherche: %d
		Dépot de ravitaillement: %d
		Silot de missiles: %d
		Usine de nanites: %d
		Terraformeur: %d
		Dock Spatial: %d

		Research:

		Technologie énergietique: %d
		Technologie Laser: %d
		Technologie à ions: %d
		Technologie hyperespace: %d
		Technologie Plasma: %d

		Réacteur à combustion: %d
		Réacteur à impulsion: %d
		Propulsion hyperespace: %d

		Technologie Espionnage: %d
		Technologie Ordinateur: %d
		Astrophysique: %d
		Réseau de recherche intergalactique: %d

		Technologie Graviton: %d

		Technologie Arme: %d
		Technologie Bouclier: %d
		Technologie Protection des vaisseaux spatiaux: %d

		Ships:

		Satellite solaire: %d
		Foreuse: %d

		Chasseur léger: %d
		Chasseur lourd: %d
		Croiseur: %d
		Vaisseau de bataille: %d
		Traqueur: %d
		Bombardier: %d
		Destructeur: %d
		Etoile de la mort: %d
		Faucheur: %d
		Eclaireur: %d

		Petit transporteur: %d
		Grand transporteur: %d
		Vaisseau de colonisation: %d
		Recycleur: %d
		Sonde d'espionnage: %d


		Defence:
		Lanceur de missiles: %d
		Artillerie laser légère: %d
		Artillerie laser lourde: %d
		Canon de Gauss: %d
		Artillerie à ions: %d
		Lanceur de plasma: %d
		Petit bouclier: %d
		Grand bouclier: %d

		Missile d'interception: %d
		Missile interplanétaire: %d
		`,

			structJSON.Data.Defender.Name,
			structJSON.Data.Defender.Planet.Coordinates,
			trashsim.MapClass[strconv.Itoa(structJSON.Data.Defender.Class)],

			structJSON.Data.Defender.Buildings["1"].Level,
			structJSON.Data.Defender.Buildings["2"].Level,
			structJSON.Data.Defender.Buildings["3"].Level,
			structJSON.Data.Defender.Buildings["4"].Level,
			structJSON.Data.Defender.Buildings["12"].Level,
			structJSON.Data.Defender.Buildings["22"].Level,
			structJSON.Data.Defender.Buildings["23"].Level,
			structJSON.Data.Defender.Buildings["24"].Level,
			structJSON.Data.Defender.Buildings["14"].Level,
			structJSON.Data.Defender.Buildings["21"].Level,
			structJSON.Data.Defender.Buildings["31"].Level,
			structJSON.Data.Defender.Buildings["34"].Level,
			structJSON.Data.Defender.Buildings["44"].Level,
			structJSON.Data.Defender.Buildings["15"].Level,
			structJSON.Data.Defender.Buildings["33"].Level,
			structJSON.Data.Defender.Buildings["36"].Level,

			structJSON.Data.Defender.Research["113"].Level,
			structJSON.Data.Defender.Research["120"].Level,
			structJSON.Data.Defender.Research["121"].Level,
			structJSON.Data.Defender.Research["114"].Level,
			structJSON.Data.Defender.Research["122"].Level,
			structJSON.Data.Defender.Research["115"].Level,
			structJSON.Data.Defender.Research["117"].Level,
			structJSON.Data.Defender.Research["118"].Level,
			structJSON.Data.Defender.Research["106"].Level,
			structJSON.Data.Defender.Research["108"].Level,
			structJSON.Data.Defender.Research["124"].Level,
			structJSON.Data.Defender.Research["123"].Level,
			structJSON.Data.Defender.Research["199"].Level,
			structJSON.Data.Defender.Research["109"].Level,
			structJSON.Data.Defender.Research["110"].Level,
			structJSON.Data.Defender.Research["111"].Level,

			structJSON.Data.Defender.Ships["212"].Count,
			structJSON.Data.Defender.Ships["217"].Count,
			structJSON.Data.Defender.Ships["204"].Count,
			structJSON.Data.Defender.Ships["205"].Count,
			structJSON.Data.Defender.Ships["206"].Count,
			structJSON.Data.Defender.Ships["207"].Count,
			structJSON.Data.Defender.Ships["215"].Count,
			structJSON.Data.Defender.Ships["211"].Count,
			structJSON.Data.Defender.Ships["213"].Count,
			structJSON.Data.Defender.Ships["214"].Count,
			structJSON.Data.Defender.Ships["218"].Count,
			structJSON.Data.Defender.Ships["219"].Count,
			structJSON.Data.Defender.Ships["202"].Count,
			structJSON.Data.Defender.Ships["203"].Count,
			structJSON.Data.Defender.Ships["208"].Count,
			structJSON.Data.Defender.Ships["209"].Count,
			structJSON.Data.Defender.Ships["210"].Count,

			structJSON.Data.Defender.Defence["401"].Count,
			structJSON.Data.Defender.Defence["402"].Count,
			structJSON.Data.Defender.Defence["403"].Count,
			structJSON.Data.Defender.Defence["404"].Count,
			structJSON.Data.Defender.Defence["405"].Count,
			structJSON.Data.Defender.Defence["406"].Count,
			structJSON.Data.Defender.Defence["407"].Count,
			structJSON.Data.Defender.Defence["408"].Count,
			structJSON.Data.Defender.Defence["502"].Count,
			structJSON.Data.Defender.Defence["503"].Count,
		)

		s.ChannelMessageSend(m.ChannelID, formattedMessage)
	})

	cmdReport.Register()
}
