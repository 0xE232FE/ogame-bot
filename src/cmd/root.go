package cmd

import (
	"fmt"
	"log"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	cfg "gitlab.com/lilflip/ogame-bot/src/configuration"
	"gitlab.com/lilflip/ogame-bot/src/core"
)

var (
	cfgFile string
	token   string
	rootCmd = &cobra.Command{
		Run: func(cmd *cobra.Command, args []string) { core.Run() },
	}
)

func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.ogame-bot.yaml)")

	rootCmd.PersistentFlags().StringVarP(&token, "token", "t", "", "token")
	cfg.Global.BindPFlag("token", rootCmd.PersistentFlags().Lookup("token"))
}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		cfg.Global.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			log.Fatal(err.Error())
		}

		// Search config in home directory with name ".ogame-bot" (without extension).
		cfg.Global.AddConfigPath(home)
		cfg.Global.SetConfigName(".ogame-bot")
	}

	cfg.Global.AutomaticEnv()

	if err := cfg.Global.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

}
