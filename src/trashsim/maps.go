package trashsim

var (
	MapClass = map[string]string{
		"1": "collecteur",
		"2": "general",
		"3": "explorateur",
	}

	MapBuildings = map[string]string{
		"1":  "Mine de métal",
		"2":  "Mine de cristal",
		"3":  "Synthétiseur de deutérium",
		"4":  "Centrale électrique solaire",
		"12": "Centrale électrique fusion",
		"14": "Usine de robots",
		"15": "Usine de nanites",
		"21": "Chantier spatial",
		"22": "Hangar de métal",
		"23": "Hangar de cristal",
		"24": "Réservoir de deutérium",
		"31": "Laboratoire de recherche",
		"33": "Terraformeur",
		"34": "Dépôt de ravitaillement",
		"36": "Dock Spatial",
		"44": "Silo de missiles",
	}

	MapResearch = map[string]string{
		"106": "Technologie Espionnage",
		"108": "Technologie Ordinateur",
		"109": "Technologie Armes",
		"110": "Technologie Bouclier",
		"111": "Technologie Protection des vaisseaux spatiaux",
		"113": "Technologie énergétique",
		"114": "Technologie Hyperespace",
		"115": "Réacteur à combustion",
		"117": "Réacteur à impulsion",
		"118": "Propulsion hyperespace",
		"120": "Technologie Laser",
		"121": "Technologie à ions",
		"122": "Technologie Plasma",
		"123": "Réseau de recherche",
		"124": "Astrophysique",
		"199": "Graviton",
	}

	MapShips = map[string]string{
		"202": "Petit Transporteur",
		"203": "Grand Transporteur",
		"204": "Chasseur Léger",
		"205": "Chasseur Lourd",
		"206": "Croiseur",
		"207": "Vaisseau de Bataille",
		"208": "Vaisseau de Colonisation",
		"209": "Recycleur",
		"210": "Sondes d'espionnage",
		"211": "Bombardier",
		"212": "Satellite Solaire",
		"213": "Destructeur",
		"215": "Traqueur",
		"217": "Foreuse",
		"218": "Faucheur",
		"214": "Etoile de la mort",
		"219": "Eclaireur",
	}

	MapDefence = map[string]string{
		"401": "Lanceur de Missile",
		"402": "Artillerie Laser Légère",
		"403": "Artillerie Laser Lourde",
		"404": "Canon de Gauss",
		"405": "Artillerie à Ions",
		"406": "Lanceur de Plasma",
		"407": "Petit Bouclier",
		"408": "Grand Bouclier",
		"502": "Missile d'Interception",
		"503": "Missile Interplanétaire",
	}
)
