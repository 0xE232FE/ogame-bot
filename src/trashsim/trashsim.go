package trashsim

type Payload struct {
	Data Data `json:"data,omitempty"`

	Party string `json:"party,omitempty"`

	Fleet int `json:"fleet,omitempty"`

	Time float32 `json:"time,omitempty"`

	Server Server `json:"server,omitempty"`
}

type Data struct {
	Activity          int      `json:"activity,omitempty"`
	Attacker          Attacker `json:"attacker,omitempty"`
	Defender          Defender `json:"defender,omitempty"`
	LootPercentage    int      `json:"loot_percentage,omitempty"`
	SpyFailChance     int      `json:"spy_fail_chance,omitempty"`
	TotalDefenseCount int      `json:"total_defense_count,omitempty"`
	TotalShipCount    int      `json:"total_ship_count,omitempty"`
	ID                int      `json:"id,omitempty"`
	Time              string   `json:"time,omitempty"`
	Timestamp         int      `json:"timestamp,omitempty"`
}

type RapidfireFrom map[string]int

type RapidfireAgainst map[string]int

type ShipsTechnology struct {
	Speed         int `json:"speed,omitempty"`
	CargoCapacity int `json:"cargo_capacity,omitempty"`
	FuelUsage     int `json:"fuel_usage,omitempty"`
	FightTechnology
	RapidfireFrom    RapidfireFrom    `json:"rapidfire_from,omitempty"`
	RapidfireAgainst RapidfireAgainst `json:"rapidfire_against,omitempty"`
}

type FightTechnology struct {
	Armour int `json:"armour,omitempty"`
	Shield int `json:"shield,omitempty"`
	Weapon int `json:"weapon,omitempty"`
}

type DefenceTechnology struct {
	Speed            int              `json:"speed,omitempty"`
	CargoCapacity    int              `json:"cargo_capacity,omitempty"`
	FuelUsage        int              `json:"fuel_usage,omitempty"`
	Armour           int              `json:"armour,omitempty"`
	Shield           int              `json:"shield,omitempty"`
	Weapon           int              `json:"weapon,omitempty"`
	RapidfireFrom    RapidfireFrom    `json:"rapidfire_from,omitempty"`
	RapidfireAgainst RapidfireAgainst `json:"rapidfire_against,omitempty"`
}

type Attacker struct {
	Planet   Planet   `json:"planet,omitempty"`
	Alliance Alliance `json:"alliance,omitempty"`
	ID       int      `json:"id,omitempty"`
	Name     string   `json:"name,omitempty"`
	Class    int      `json:"class,omitempty"`
}

type Planet struct {
	Type        int    `json:"type,omitempty"`
	Name        string `json:"name,omitempty"`
	Coordinates string `json:"coordinates,omitempty"`
	Galaxy      int    `json:"galaxy,omitempty"`
	System      int    `json:"system,omitempty"`
	Position    int    `json:"position,omitempty"`
}

type Alliance struct {
	Tag  string `json:"tag,omitempty"`
	Name string `json:"name,omitempty"`
}

type Defender struct {
	Resources Resources           `json:"resources,omitempty"`
	Buildings map[string]Building `json:"buildings,omitempty"`
	Research  map[string]Research `json:"research,omitempty"`
	Ships     map[string]Ship     `json:"ships,omitempty"`
	Defence   map[string]Defence  `json:"defence,omitempty"`
	Planet    Planet              `json:"planet,omitempty"`
	ID        string              `json:"id,omitempty"`
	Name      string              `json:"name,omitempty"`
	Class     int                 `json:"class,omitempty"`
}

type Resources struct {
	Metal     int `json:"metal,omitempty"`
	Crystal   int `json:"crystal,omitempty"`
	Deuterium int `json:"deuterium,omitempty"`
	Energy    int `json:"energy,omitempty"`
}

type Building struct {
	Level      int                `json:"level,omitempty"`
	Technology BuildingTechnology `json:"technology,omitempty"`
}

type BuildingTechnology struct {
	PowerBase float32   `json:"power_base,omitempty"`
	Type      int       `json:"type,omitempty"`
	Resources Resources `json:"resources,omitempty"`
}

type Research struct {
	Level      int                `json:"level,omitempty"`
	Technology BuildingTechnology `json:"technology,omitempty"`
}

type Ship struct {
	Count      int             `json:"count,omitempty"`
	Technology ShipsTechnology `json:"technology,omitempty"`
}

type Defence struct {
	Count      int             `json:"count,omitempty"`
	Technology FightTechnology `json:"technology,omitempty"`
}

type Server struct {
	Name                     string
	Number                   string
	Language                 string
	Timezone                 string
	TimezoneOffset           string
	Domain                   string
	Version                  string
	Speed                    string
	SpeedFleet               string
	Galaxies                 string
	Systems                  string
	ACS                      string
	RapideFire               string
	DeefToTF                 string
	DebrisFactor             string
	DebrisFactorDef          string
	RepairFactor             string
	NewbieProtectionLimit    string
	NewbieProtectionHigh     string
	TopScore                 string
	BonusFields              string
	DonutGalaxy              string
	DonutSystem              string
	WFEnabled                string
	WFMinResLost             string
	WFMinLossPercent         string
	WFBasicPercentRepairable string
}
